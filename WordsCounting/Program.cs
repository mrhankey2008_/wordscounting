﻿using System;
using Autofac;
using CommandLine;
using WordsCounting.CommandLineOptions;

namespace WordsCounting
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                Parser.Default.ParseArguments<Options>(args)
                    .WithParsed(Run);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.Write("Press any key to exit...");
            Console.Read();
        }

        private static void Run(Options options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));

            using (var container = ContainerFactory.BuildContainer(options))
            {
                var service = container.Resolve<WordCountingService>();
                service.Run();
            }
        }
    }
}