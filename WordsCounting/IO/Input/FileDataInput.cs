﻿using System;
using System.IO;
using WordsCounting.CommandLineOptions;

namespace WordsCounting.IO.Input
{
    internal sealed class FileDataInput : IDataInput
    {
        public const string Name = "FileDataInput";

        public const string Description = "Input data into file";

        private readonly IFileDataInputOptions _options;

        public FileDataInput(IFileDataInputOptions options)
        {
            _options = options ?? throw new ArgumentNullException(nameof(options));
        }

        public string Read()
        {
            ThrowIfFileCantBeRead();
            var textFromFile = File.ReadAllText(_options.SourceFile);
            Console.WriteLine($"Text from file {_options.SourceFile}");
            Console.WriteLine(textFromFile);
            return textFromFile;
        }

        private void ThrowIfFileCantBeRead()
        {
            if (!File.Exists(_options.SourceFile))
            {
                throw new IOException("File is not exists");
            }
        }
    }
}