﻿using System;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using WordsCounting.CommandLineOptions;

namespace WordsCounting.IO.Input
{
    internal sealed class DatabaseDataInput : IDataInput
    {
        public const string Name = "DatabaseDataInput";

        public const string Description = "Input data into database";

        private readonly IDatabaseDataOptions _options;

        public DatabaseDataInput(IDatabaseDataOptions options)
        {
            _options = options ?? throw new ArgumentNullException(nameof(options));
        }

        public string Read()
        {
            ThrowIfCantConnectionDb();
            ThrowIfNotExistsTableTexts();
            ThrowIfNotExistsColumnTextInTableTexts();
            using (var connection = new SqlConnection(_options.ConnectionString))
            {
                var textObj = connection.QueryFirstOrDefault("Select * FROM Texts");
                if (textObj == null)
                {
                    throw new NullReferenceException("Record does not exists in table 'Texts'");
                }
                return textObj.Text;
            }
        }

        private void ThrowIfCantConnectionDb()
        {
            using (var connection = new SqlConnection(_options.ConnectionString))
            {
                if (connection.State == ConnectionState.Broken)
                {
                    throw new Exception("Cannot connect to db! Check connection string!");
                }
            }
        }

        private void ThrowIfNotExistsTableTexts()
        {
            using (var connection = new SqlConnection(_options.ConnectionString))
            {
                var sqlQuery = @"SELECT CASE WHEN EXISTS (
                                    SELECT *
                                    FROM INFORMATION_SCHEMA.TABLES
                                    WHERE TABLE_NAME = N'Texts'
                                 )
                                 THEN CAST(1 AS BIT)
                                 ELSE CAST(0 AS BIT) END";
                var isExistTableTexts = connection.ExecuteScalar<bool>(sqlQuery);
                if (!isExistTableTexts)
                {
                    throw new Exception("Table Texts does not exist!");
                }
            }
        }

        private void ThrowIfNotExistsColumnTextInTableTexts()
        {
            using (var connection = new SqlConnection(_options.ConnectionString))
            {
                var sqlQuery = @"SELECT CASE WHEN EXISTS(
                                     SELECT *
                                     FROM sys.columns 
                                     WHERE Name = N'Text' 
                                     AND Object_ID = Object_ID(N'Texts')
                                 )
                                THEN CAST(1 AS BIT)
                                ELSE CAST(0 AS BIT) END";
                var isExistColumnTextInTableTexts = connection.ExecuteScalar<bool>(sqlQuery);
                if (!isExistColumnTextInTableTexts)
                {
                    throw new Exception("Column 'Text' in table 'Texts' does not exist!");
                }
            }
        }
    }
}