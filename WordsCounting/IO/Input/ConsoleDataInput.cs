﻿using System;

namespace WordsCounting.IO.Input
{
    internal sealed class ConsoleDataInput : IDataInput
    {
        public const string Name = "ConsoleDataInput";

        public const string Description = "Input data into console";

        public string Read()
        {
            Console.WriteLine("Input text:");
            return Console.ReadLine();
        }
    }
}