﻿namespace WordsCounting.IO.Input
{
    internal interface IDataInput
    {
        string Read();
    }
}