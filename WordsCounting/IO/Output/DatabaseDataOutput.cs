﻿using System;
using System.Data.SqlClient;
using Dapper;
using WordsCounting.CommandLineOptions;

namespace WordsCounting.IO.Output
{
    internal sealed class DatabaseDataOutput : IDataOutput
    {
        public const string Name = "DatabaseDataOutput";

        public const string Description = "Output data from database";

        private readonly IDatabaseDataOptions _options;

        public DatabaseDataOutput(IDatabaseDataOptions options)
        {
            _options = options ?? throw new ArgumentNullException(nameof(options));
        }

        public void Save(long count)
        {
            EnsureDatabase();
            using (var connection = new SqlConnection(_options.ConnectionString))
            {
                var sqlQueryInsertCountWords = $@"INSERT INTO dbo.Texts
                                                (Text, CountWords)
                                                VALUES ('', {count});";
                connection.Execute(sqlQueryInsertCountWords);
            }
        }

        private void EnsureDatabase()
        {
            var serverName =
                GetValueFromConnectionStringByKey(_options.ConnectionString, "Data Source", "Server");
            if (string.IsNullOrEmpty(serverName))
            {
                throw new Exception("ServerName is empty!");
            }

            var dataBaseName =
                GetValueFromConnectionStringByKey(_options.ConnectionString, "Database", "Initial Catalog");
            if (string.IsNullOrEmpty(dataBaseName))
            {
                throw new Exception("DatabaseName is empty!");
            }

            using (var connection =
                new SqlConnection($"Server={serverName};" +
                                  $"Database=Master;" +
                                  $"Integrated Security=True"))
            {

                var sqlQueryCreateDatabase = $@"IF NOT EXISTS 
                                                (SELECT 1 FROM sys.databases WHERE name = N'{dataBaseName}')
                                                BEGIN
                                                  CREATE DATABASE dataBaseName
                                                END;";
                connection.Execute(sqlQueryCreateDatabase);

                var sqlQueryCreateTable = $@"IF NOT EXISTS 
                                             (SELECT * FROM {dataBaseName}.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'Texts')
                                             BEGIN
                                                CREATE TABLE {dataBaseName}.dbo.Texts (
                                                    Id INT IDENTITY
                                                    ,Text TEXT NULL
                                                    ,CountWords INT NULL
                                                    ,CONSTRAINT PK_Articles_Id PRIMARY KEY CLUSTERED (Id)
                                                )
                                             END;";
                connection.Execute(sqlQueryCreateTable);
            }
            Console.WriteLine("Count words successful write to database");
        }

        private string GetValueFromConnectionStringByKey(string connectionString, params string[] keys)
        {
            var builder = new System.Data.Common.DbConnectionStringBuilder();
            builder.ConnectionString = connectionString;

            for (int i = 0; i < keys.Length; i++)
            {
                if (builder.TryGetValue(keys[i], out var resValue))
                {
                    return resValue.ToString();
                }
            }

            return null;
        }
    }
}