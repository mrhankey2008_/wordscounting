﻿namespace WordsCounting.IO.Output
{
    internal interface IDataOutput
    {
        void Save(long count);
    }
}