﻿using System;
using System.IO;
using WordsCounting.CommandLineOptions;

namespace WordsCounting.IO.Output
{
    internal sealed class FileDataOutput : IDataOutput
    {
        public const string Name = "FileDataOutput";

        public const string Description = "Output data from file";

        private readonly IFileDataOutputOptions _options;

        public FileDataOutput(IFileDataOutputOptions options)
        {
            _options = options ?? throw new ArgumentNullException(nameof(options));
        }

        public void Save(long count)
        {
            EnsureFile();
            File.WriteAllText(_options.DestinationFile, $"total words = {count}");
            Console.WriteLine($"Count words save to file {_options.DestinationFile}");
        }

        private void EnsureFile()
        {
            if (!File.Exists(_options.DestinationFile))
            {
                using (File.Create(_options.DestinationFile))
                {
                    //do nothing       
                }
            }
        }

    }
}