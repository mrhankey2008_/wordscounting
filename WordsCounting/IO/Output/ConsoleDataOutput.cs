﻿using System;

namespace WordsCounting.IO.Output
{
    internal sealed class ConsoleDataOutput : IDataOutput
    {
        public const string Name = "ConsoleDataOutput";

        public const string Description = "Output data from console";

        public void Save(long count)
        {
            Console.WriteLine($"total words = {count}");
        }
    }
}