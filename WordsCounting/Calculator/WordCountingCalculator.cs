using System;
using NLog;
using WordsCounting.CalculationWordStrategy;
using WordsCounting.IO.Input;
using WordsCounting.IO.Output;

namespace WordsCounting.Calculator
{
    internal sealed class WordCountingCalculator
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly IDataInput _dataInput;
        private readonly IDataOutput _dataOutput;
        private readonly ICalculationWordStrategy _strategy;

        public WordCountingCalculator(
            IDataInput dataInput,
            IDataOutput dataOutput,
            ICalculationWordStrategy strategy)
        {
            _dataInput = dataInput ?? throw new ArgumentNullException(nameof(dataInput));
            _dataOutput = dataOutput ?? throw new ArgumentNullException(nameof(dataOutput));
            _strategy = strategy ?? throw new ArgumentNullException(nameof(strategy));
        }

        public void Calculate()
        {
            try
            {
                var text = _dataInput.Read();
                var count = _strategy.Calculate(text);
                _dataOutput.Save(count);
            }
            catch (Exception e)
            {
                _logger.Error(e, "An error occurred while calculating the number of words!");
                throw new Exception(e.Message);
            }
        }
    }
}