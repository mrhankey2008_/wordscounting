﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordsCounting.IO.Input;

namespace WordsCounting.Providers
{
    internal sealed class DataInputProvider : IDataInputProvider
    {
        private readonly IEnumerable<Lazy<IDataInput, InputMetadata>> _dataInputs;

        public DataInputProvider(IEnumerable<Lazy<IDataInput, InputMetadata>> dataInputs)
        {
            _dataInputs = dataInputs ?? throw new ArgumentNullException(nameof(dataInputs));
        }

        public IDataInput GetDataInput(string dataInputName)
        {
            if (dataInputName == null) throw new ArgumentNullException(nameof(dataInputName));

            var lazyDataInput = _dataInputs.FirstOrDefault(di => di.Metadata?.Name == dataInputName);

            if (lazyDataInput == null)
            {
                throw new NullReferenceException($"Does not exist dataInput: {dataInputName}");
            }

            return lazyDataInput.Value;
        }
    }
}
