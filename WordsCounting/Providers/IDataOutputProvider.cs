﻿using WordsCounting.IO.Output;

namespace WordsCounting.Providers
{
    internal interface IDataOutputProvider
    {
        IDataOutput GetDataOutput(string dataOutputName);
    }
}