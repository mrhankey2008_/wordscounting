﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordsCounting.CalculationWordStrategy;

namespace WordsCounting.Providers
{
    internal sealed class CalculationWordStrategyProvider : ICalculationWordStrategyProvider
    {
        private readonly IEnumerable<Lazy<ICalculationWordStrategy, StrategyMetadata>> _strategies;

        public CalculationWordStrategyProvider(IEnumerable<Lazy<ICalculationWordStrategy, StrategyMetadata>> strategies)
        {
            _strategies = strategies ?? throw new ArgumentNullException(nameof(strategies));
        }


        public ICalculationWordStrategy GetStrategy(string strategyName)
        {
            if (strategyName == null) throw new ArgumentNullException(nameof(strategyName));

            var lazyStrategy = _strategies.FirstOrDefault(s => s.Metadata?.Name == strategyName);

            if (lazyStrategy == null)
            {
                throw new NullReferenceException($"Does not exist strategy: {strategyName}");
            }

            return lazyStrategy.Value;
        }
    }
}
