﻿using WordsCounting.IO.Input;

namespace WordsCounting.Providers
{
    internal interface IDataInputProvider
    {
        IDataInput GetDataInput(string dataInputName);
    }
}