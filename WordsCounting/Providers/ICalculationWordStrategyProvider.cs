﻿using WordsCounting.CalculationWordStrategy;

namespace WordsCounting
{
    internal interface ICalculationWordStrategyProvider
    {
        ICalculationWordStrategy GetStrategy(string strategyName);
    }
}