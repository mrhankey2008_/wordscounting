﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordsCounting.IO.Output;

namespace WordsCounting.Providers
{
    internal sealed class DataOutputProvider : IDataOutputProvider
    {
        private readonly IEnumerable<Lazy<IDataOutput, OutputMetadata>> _dataOutputs;

        public DataOutputProvider(IEnumerable<Lazy<IDataOutput, OutputMetadata>> dataOutputs)
        {
            _dataOutputs = dataOutputs ?? throw new ArgumentNullException(nameof(dataOutputs));
        }

        public IDataOutput GetDataOutput(string dataOutputName)
        {
            if (dataOutputName == null) throw new ArgumentNullException(nameof(dataOutputName));

            var lazyDataOutput = _dataOutputs.FirstOrDefault(di => di.Metadata?.Name == dataOutputName);

            if (lazyDataOutput == null)
            {
                throw new NullReferenceException($"Does not exist dataOutput: {dataOutputName}");
            }

            return lazyDataOutput.Value;
        }
    }
}
