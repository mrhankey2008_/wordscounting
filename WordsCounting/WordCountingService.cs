﻿using System;
using WordsCounting.Calculator;
using WordsCounting.CommandLineOptions;
using WordsCounting.Providers;

namespace WordsCounting
{
    internal sealed class WordCountingService
    {
        private readonly Options _options;

        private readonly ICalculationWordStrategyProvider _calculationWordStrategyProvider;

        private readonly IDataInputProvider _dataInputProvider;

        private readonly IDataOutputProvider _dataOutputProvider;

        public WordCountingService(
            Options options,
            ICalculationWordStrategyProvider calculationWordStrategyProvider,
            IDataInputProvider dataInputProvider,
            IDataOutputProvider dataOutputProvider)
        {
            _options = options ?? throw new ArgumentNullException(nameof(options));
            _calculationWordStrategyProvider = calculationWordStrategyProvider ?? throw new ArgumentNullException(nameof(calculationWordStrategyProvider));
            _dataInputProvider = dataInputProvider ?? throw new ArgumentNullException(nameof(dataInputProvider));
            _dataOutputProvider = dataOutputProvider ?? throw new ArgumentNullException(nameof(dataOutputProvider));
        }

        public void Run()
        {
            var dataInput = _dataInputProvider.GetDataInput(_options.Input);
            var dataOutput = _dataOutputProvider.GetDataOutput(_options.Output);
            var strategy = _calculationWordStrategyProvider.GetStrategy(_options.Strategy);
            var wordCountingCalculator = new WordCountingCalculator(dataInput, dataOutput, strategy);
            wordCountingCalculator.Calculate();
        }
    }
}
