﻿using System;

namespace WordsCounting.CalculationWordStrategy
{
    internal sealed class SimpleCalculationWordStrategy : ICalculationWordStrategy
    {
        public const string Name = "SimpleCalculationWordStrategy";
        
        public const string Description = "Count words with loop by symbol space";

        public long Calculate(string text)
        {
            if (text == null) throw new ArgumentNullException(nameof(text));

            int wordCount = 0, index = 0;

            while (index < text.Length)
            {
                while (index < text.Length && !char.IsWhiteSpace(text[index]))
                {
                    index++;
                }

                wordCount++;

                while (index < text.Length && char.IsWhiteSpace(text[index]))
                {
                    index++;
                }
            }

            return wordCount;
        }
    }
}