﻿using System;
using System.Text.RegularExpressions;

namespace WordsCounting.CalculationWordStrategy
{
    internal sealed class RegexCalculationWordStrategy : ICalculationWordStrategy
    {
        public const string Name = "RegexCalculationWordStrategy";
        
        public const string Description = "Count words with Regex";    
        
        public long Calculate(string text)
        {
            if (text == null) throw new ArgumentNullException(nameof(text));

            MatchCollection collection = Regex.Matches(text, @"[\S]+");
            return collection.Count;
        }
    }
}