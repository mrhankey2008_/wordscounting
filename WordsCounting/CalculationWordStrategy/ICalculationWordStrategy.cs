﻿namespace WordsCounting.CalculationWordStrategy
{
    internal interface ICalculationWordStrategy
    {
        long Calculate(string text);
    }
}