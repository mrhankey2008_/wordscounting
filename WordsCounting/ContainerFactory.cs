﻿using System;
using Autofac;
using WordsCounting.CalculationWordStrategy;
using WordsCounting.CommandLineOptions;
using WordsCounting.IO.Input;
using WordsCounting.IO.Output;
using WordsCounting.Providers;

namespace WordsCounting
{
    internal static class ContainerFactory
    {
        public static IContainer BuildContainer(Options options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));

            var builder = new ContainerBuilder();

            builder.RegisterInstance(options)
                .SingleInstance()
                .AsImplementedInterfaces()
                .AsSelf();

            builder.RegisterType<RegexCalculationWordStrategy>()
                .AsImplementedInterfaces()
                .WithMetadata<BaseMetadata>(m => m
                    .For(am => am.Name, RegexCalculationWordStrategy.Name)
                    .For(am => am.Description, RegexCalculationWordStrategy.Description))
                .AsSelf();

            builder.RegisterType<SimpleCalculationWordStrategy>()
                .AsImplementedInterfaces()
                .WithMetadata<StrategyMetadata>(m => m
                    .For(am => am.Name, SimpleCalculationWordStrategy.Name)
                    .For(am => am.Description, SimpleCalculationWordStrategy.Description))
                .AsSelf();

            builder.RegisterType<ConsoleDataInput>()
                .AsImplementedInterfaces()
                .WithMetadata<InputMetadata>(m => m
                    .For(am => am.Name, ConsoleDataInput.Name)
                    .For(am => am.Description, ConsoleDataInput.Description))
                .AsSelf();

            builder.RegisterType<DatabaseDataInput>()
                .AsImplementedInterfaces()
                .WithMetadata<InputMetadata>(m => m
                    .For(am => am.Name, DatabaseDataInput.Name)
                    .For(am => am.Description, DatabaseDataInput.Description))
                .AsSelf();

            builder.RegisterType<FileDataInput>()
                .AsImplementedInterfaces()
                .WithMetadata<InputMetadata>(m => m
                    .For(am => am.Name, FileDataInput.Name)
                    .For(am => am.Description, FileDataInput.Description))
                .AsSelf();

            builder.RegisterType<ConsoleDataOutput>()
                .AsImplementedInterfaces()
                .WithMetadata<InputMetadata>(m => m
                    .For(am => am.Name, ConsoleDataOutput.Name)
                    .For(am => am.Description, ConsoleDataOutput.Description))
                .AsSelf();

            builder.RegisterType<DatabaseDataOutput>()
                .AsImplementedInterfaces()
                .WithMetadata<InputMetadata>(m => m
                    .For(am => am.Name, DatabaseDataOutput.Name)
                    .For(am => am.Description, DatabaseDataOutput.Description))
                .AsSelf();

            builder.RegisterType<FileDataOutput>()
                .AsImplementedInterfaces()
                .WithMetadata<InputMetadata>(m => m
                    .For(am => am.Name, FileDataOutput.Name)
                    .For(am => am.Description, FileDataOutput.Description))
                .AsSelf();

            builder.RegisterType<CalculationWordStrategyProvider>()
                .AsImplementedInterfaces()
                .AsSelf();

            builder.RegisterType<DataInputProvider>()
                .AsImplementedInterfaces()
                .AsSelf();

            builder.RegisterType<DataOutputProvider>()
                .AsImplementedInterfaces()
                .AsSelf();

            builder.RegisterType<WordCountingService>()
                .AsImplementedInterfaces()
                .AsSelf();

            return builder.Build();
        }
    }
}