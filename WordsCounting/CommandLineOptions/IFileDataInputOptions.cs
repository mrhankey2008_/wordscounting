﻿namespace WordsCounting.CommandLineOptions
{
    internal interface IFileDataInputOptions
    {
        string SourceFile { get; }
    }
}