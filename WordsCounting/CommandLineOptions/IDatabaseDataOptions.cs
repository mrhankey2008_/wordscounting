﻿namespace WordsCounting.CommandLineOptions
{
    internal interface IDatabaseDataOptions
    {
        string ConnectionString { get; }
    }
}