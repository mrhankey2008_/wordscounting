﻿using CommandLine;

namespace WordsCounting.CommandLineOptions
{
    internal class Options : IFileDataInputOptions, IFileDataOutputOptions, IDatabaseDataOptions
    {
        [Option('s', "strategy", Required = true, HelpText = "Name strategy")]
        public string Strategy { get; set; }

        [Option('i', "input", Required = true, HelpText = "Name input")]
        public string Input { get; set; }

        [Option('o', "output", Required = true, HelpText = "Name output")]
        public string Output { get; set; }

        [Option('f', "source-file", Required = false, HelpText = "SourceFile")]
        public string SourceFile { get; set; }

        [Option('c', "connection-string", Required = false, HelpText = "ConnectionString")]
        public string ConnectionString { get; set; }

        [Option('d', "destination-file", Required = false, HelpText = "DestinationFile")]
        public string DestinationFile { get; set; }

        //s -SimpleCalculationWordStrategy i -Console o -Console 
    }
}
