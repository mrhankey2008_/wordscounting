﻿namespace WordsCounting.CommandLineOptions
{
    public interface IFileDataOutputOptions
    {
        string DestinationFile { get; }
    }
}