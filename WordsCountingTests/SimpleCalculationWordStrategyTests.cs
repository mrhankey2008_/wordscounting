﻿using System;
using FluentAssertions;
using NUnit.Framework;
using WordsCounting.CalculationWordStrategy;

namespace WordsCountingTests
{
    [TestFixture]
    public class SimpleCalculationWordStrategyTests
    {
        [Test]
        [TestCase(3, "my little pony")]
        [TestCase(3, "my little \r pony")]
        [TestCase(3, "my little  pony")]
        [TestCase(0, "")]
        public void ShouldBeCalculateCountNumberOfWordsWhenTextIsNotNull(long count, string text)
        {
            var calculator = new SimpleCalculationWordStrategy();

            var result = calculator.Calculate(text);

            result.Should().Be(count);
        }


        [Test]
        public void ShouldBeThrowArgumentNullExceptionWhenTextIsNull()
        {
            var calculator = new SimpleCalculationWordStrategy();

            var action = new Func<long>(() => calculator.Calculate(null));

            action.Should().Throw<ArgumentNullException>();
        }

    }
}