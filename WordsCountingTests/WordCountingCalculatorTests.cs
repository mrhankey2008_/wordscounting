﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using WordsCounting.CalculationWordStrategy;
using WordsCounting.Calculator;
using WordsCounting.IO.Input;
using WordsCounting.IO.Output;

namespace WordsCountingTests
{
    [TestFixture]
    public class WordCountingCalculatorTests
    {
        [Test]
        public void ShouldBeOnceReadTextDataInputWhenCalculate()
        {
            var dataInputMock = new Mock<IDataInput>();

            var calculator = new WordCountingCalculator(
                dataInputMock.Object,
                Mock.Of<IDataOutput>(),
                Mock.Of<ICalculationWordStrategy>());

            calculator.Calculate();

            dataInputMock.Verify(x => x.Read(), Times.Once());
        }

        [Test]
        public void ShouldBeReadTextAndUseItForCalculationWordStrategy()
        {
            const string sampleText = "my text";

            var dataInput = new Mock<IDataInput>();
            dataInput.Setup(x => x.Read()).Returns(sampleText);

            var strategyMock = new Mock<ICalculationWordStrategy>();

            var calculator = new WordCountingCalculator(
                dataInput.Object,
                Mock.Of<IDataOutput>(),
                strategyMock.Object);

            calculator.Calculate();

            strategyMock.Verify(x => x.Calculate(sampleText));
        }

        [Test]
        public void ShouldBeOnceSaveCountWordsWhenCalculate()
        {
            var dataOutputMock = new Mock<IDataOutput>();

            var calculator = new WordCountingCalculator(
                Mock.Of<IDataInput>(),
                dataOutputMock.Object,
                Mock.Of<ICalculationWordStrategy>());

            calculator.Calculate();

            dataOutputMock.Verify(x => x.Save(0), Times.Once());
        }

        [Test]
        public void ShouldBeOnceCalculateCountWordsWhenCalculate()
        {
            const string sampleText = "my text";

            var dataInput = new Mock<IDataInput>();
            dataInput.Setup(x => x.Read()).Returns(sampleText);

            var calculationWordStategy = new Mock<ICalculationWordStrategy>();

            var calculator = new WordCountingCalculator(
                dataInput.Object,
                Mock.Of<IDataOutput>(),
                calculationWordStategy.Object);

            calculator.Calculate();

            calculationWordStategy.Verify(x => x.Calculate(sampleText), Times.Once);
        }

        [Test]
        public void ShouldBeCalculateCountWordsAndUseItForDataOutputSave()
        {
            var strategyMock = new Mock<ICalculationWordStrategy>();
            var resultCountWords = strategyMock.Setup(x => x.Calculate(""));
            
            var dataOutput = new Mock<IDataOutput>();

            var calculator = new WordCountingCalculator(
                Mock.Of<IDataInput>(),
                dataOutput.Object,
                strategyMock.Object);

            calculator.Calculate();

            dataOutput.Verify(x => x.Save(resultCountWords.As<long>()));
        }

    }
}
