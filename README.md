# Запуск программы #

При запуске программы необходимо передать параметры запуска.

### Параметры запуска ###

* s (Стратегия расчета слов. Возможные варианты: SimpleCalculationWordStrategy, RegexCalculationWordStrategy) Обязательный параметр
* i (Источник входных данных.  Возможные варианты: ConsoleDataInput, DatabaseDataInput, FileDataInput) Обязательный параметр
* o (Приемник выходных данных.  Возможные варианты: ConsoleDataOutput, DatabaseDataOutput, FileDataOutput) Обязательный параметр
* f (Путь до файла откуда вычитывать текст)
* c (Строка подключения к БД)
* d (Путь до файла куда записывается результат)

### Примеры параметров запуска ###

* -s SimpleCalculationWordStrategy -i ConsoleDataInput -o DatabaseDataOutput -c Server=(local);Database=WordsCountingDB;Trusted_Connection=True;
* -s RegexCalculationWordStrategy -i ConsoleDataInput -o ConsoleDataOutput
* -s RegexCalculationWordStrategy -i ConsoleDataInput -o FileDataOutput -d C:/Temp/res.txt
* и др.