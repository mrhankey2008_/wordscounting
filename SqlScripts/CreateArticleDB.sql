﻿USE master
GO

CREATE DATABASE WordsCountingDB
GO

CREATE TABLE WordsCountingDB.dbo.Texts (
  Id INT IDENTITY
 ,Text TEXT NULL
 ,CountWords INT NULL
 ,CONSTRAINT PK_Articles_Id PRIMARY KEY CLUSTERED (Id)
)
GO